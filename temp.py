#  Copyright © 2018 Anant Sujatanagarjuna
#  This file is part of Need_for_Shit.

#     Need_for_Shit is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     Need_for_Shit is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with Need_for_Shit.  If not, see <https://www.gnu.org/licenses/>.

import sys
from pymunk import (Body, Poly, moment_for_box, Vec2d, Segment, Space)
import pygame
import pymunk.pygame_util as pygame_util
from pygame.locals import *


def add_World_Bounds(world,
                     radius):
    body = Body(body_type=Body.STATIC)
    body.position = (400, 300)
    l1 = Segment(body, (-400, -300), (-400, 300), radius=radius)
    l2 = Segment(body, (-400, -300), (400, -300), radius=radius)
    l3 = Segment(body, (400, 300), (400, -300), radius=radius)
    l4 = Segment(body, (400, 300), (-400, 300), radius=radius)
    world.add(l1, l2, l3, l4)
    return (l1, l2, l3, l4)


# def to_pygame(p):
#     return int(p.x), int(-p.y+800)


# def draw_lines(world, lines):
#     for line in lines:
#         body = line.body
#         pv1 = body.position + line.a.rotated(body.angle)
#         pv2 = body.position + line.a.rotated(body.angle)
#         p1 = to_pygame(pv1)
#         p2 = to_pygame(pv2)
#         pygame.draw_lines(world, (0, 0, 0), False, [p1, p2])


def add_car(world, position):
    width = 20.0
    length = 40.0
    density = .001
    mass = width * length * density
    inertia = moment_for_box(mass, (width, length))
    body = Body(mass, inertia)
    body.position = position
    shape = Poly.create_box(body, size=(width, length))
    shape.elasticity = 0.5
    world.add(body, shape)
    return body, shape

def sign(vec1, vec2):
    if(Vec2d.dot(vec1, vec2) > 0):
        return 1
    elif(Vec2d.dot(vec1, vec2) < 0):
        return -1
    return 0

def main():
    pygame.init()
    screen = pygame.display.set_mode((800, 600))
    draw_options = pygame_util.DrawOptions(screen)
    clock = pygame.time.Clock()
    world = Space()
    world.gravity = (0.0, 0.0)
    world_bounds = add_World_Bounds(world, 5)
    car_body, car_shape = add_car(world, (400, 300))
    fw_friction = 0.1
    lat_friction = 2
    while True:
        # forward_normal = Vec2d.normalized(car_body.local_to_world((0, 1))
        #                                   - car_body.position)
        # right_normal = Vec2d.normalized(car_body.local_to_world((1, 0))
        #                                 - car_body.position)
        # car_velocity = car_body.velocity
        # local_velocity_vecs = (car_velocity.projection(right_normal),
        #                        car_velocity.projection(forward_normal))
        # local_velocity = Vec2d(
        #     (local_velocity_vecs[0].get_length() *
        #      sign(car_velocity, right_normal),
        #      local_velocity_vecs[1].get_length() *
        #      sign(car_velocity, forward_normal)))
        local_velocity = Vec2d(
            Vec2d.dot(
                car_body.velocity, car_body.rotation_vector),
            Vec2d.dot(
                car_body.velocity, car_body.rotation_vector.perpendicular()))
        print(local_velocity)
        car_body.apply_force_at_local_point(
            (0, -local_velocity.y * fw_friction),
            (0, 0))
        car_body.apply_force_at_local_point(
            (-local_velocity.x * lat_friction, 0),
            (0, 0))
        car_body.apply_force_at_local_point(
            (car_body.angular_velocity * lat_friction * 5, 0),
            (0, 40))
        # print(local_velocity)
        for event in pygame.event.get():
            if(event.type == QUIT):
                sys.exit(0)
        keys_pressed = pygame.key.get_pressed()
        if(keys_pressed[K_ESCAPE]):
            sys.exit(0)
        if(keys_pressed[K_UP]):
            car_body.apply_force_at_local_point((0, 200), (0, 0))
        if(keys_pressed[K_DOWN]):
            car_body.apply_force_at_local_point((0, -200), (0, 0))
        if(keys_pressed[K_LEFT]):
            car_body.apply_force_at_local_point((-50, 0), (0, 40))
        if(keys_pressed[K_RIGHT]):
            car_body.apply_force_at_local_point((50, 0), (0, 40))

        screen.fill((255, 255, 255))
        world.step(1/60.0)
        world.debug_draw(draw_options)
        pygame.display.update()
        clock.tick(60)


if __name__ == '__main__':
    sys.exit(main())
