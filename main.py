#  Copyright © 2018 Anant Sujatanagarjuna
#  This file is part of Need_for_Shit.

#     Need_for_Shit is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     Need_for_Shit is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with Need_for_Shit.  If not, see <https://www.gnu.org/licenses/>.
import os
import sys
import time
from math import pi as π
from mechanics.vehicle_mechanics import Vehicle
import pygame
from pymunk import (Segment, Body)
import pymunk.pygame_util
from pygame.locals import *


def add_vehicle(world,
                position,
                width=30.0,
                length=60.0,
                transmission_type=Vehicle.FWD_TRANSMISSION,
                drive_force=10):
    return Vehicle(world,
                   position,
                   mass=100,
                   front_tire_length=15,
                   front_tire_width=5,
                   front_tire_rolling_resistance=0.01,
                   front_tire_horizontal_friction=1.0,
                   rear_tire_length=15,
                   rear_tire_width=5,
                   rear_tire_rolling_resistance=0.01,
                   rear_tire_horizontal_friction=1.0,
                   width=width,
                   length=length,
                   transmission_type=transmission_type,
                   drive_force=drive_force)


def add_World_Bounds(world,
                     radius):
    body = Body(body_type=Body.STATIC)
    body.position = (400, 300)
    l1 = Segment(body, (-400, -300), (-400, 300), radius=radius)
    l2 = Segment(body, (-400, -300), (400, -300), radius=radius)
    l3 = Segment(body, (400, 300), (400, -300), radius=radius)
    l4 = Segment(body, (400, 300), (-400, 300), radius=radius)
    world.add(l1, l2, l3, l4)
    return (l1, l2, l3, l4)


def main():

    pygame.mixer.pre_init(44100, -16, 2, 2048) # setup mixer to avoid sound lag
    pygame.init()
    try:
       # pygame.mixer.music.load(os.path.join('resources/sounds', 'muscle_idle.wav'))#load music
        acceleratelow = pygame.mixer.Sound(os.path.join('resources/sounds','muscle_onlow.wav'))  #load sound
        acceleratemed = pygame.mixer.Sound(os.path.join('resources/sounds','muscle_onmid.wav'))  #load sound
        acceleratehigh = pygame.mixer.Sound(os.path.join('resources/sounds','muscle_onhigh.wav'))  #load sound
        deceleratelow = pygame.mixer.Sound(os.path.join('resources/sounds','muscle_offlow.wav'))  #load sound
        deceleratemed = pygame.mixer.Sound(os.path.join('resources/sounds','muscle_offmid.wav'))  #load sound
        deceleratehigh = pygame.mixer.Sound(os.path.join('resources/sounds','muscle_offhigh.wav'))  #load sound
        engineidle = pygame.mixer.Sound(os.path.join('resources/sounds','muscle_idle.wav'))  #load sound
    except Exception as e:
        print(e)
    screen = pygame.display.set_mode((800, 600))
    draw_options = pymunk.pygame_util.DrawOptions(screen)
    pygame.display.set_caption("Need For Shit")
    clock = pygame.time.Clock()

    world = pymunk.Space()
    world.gravity = (0.0, 0.0)
    add_World_Bounds(world, 10)
    car1 = add_vehicle(world,
                       (400, 300),
                       transmission_type=Vehicle.AWD_TRANSMISSION,
                       drive_force=20000)
    while True:
        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                sys.exit(0)
            else:
                start_time = time.time()
        keys_pressed = pygame.key.get_pressed()
        
        if(keys_pressed[K_UP]):
            engineidle.stop()
            pressedtime = time.time() - start_time
            if pressedtime < 1.2:
                acceleratelow.play()   
            elif(pressedtime > 1.2):
                acceleratelow.stop()   
                acceleratemed.play()
            elif(pressedtime > 3.2):
                acceleratemed.stop()
                acceleratehigh.play()
            
            car1.drive()
        if(keys_pressed[K_DOWN]):
            pygame.mixer.music.stop()
            acceleratelow.stop()
            deceleratelow.play()
            car1.brake()

        if(keys_pressed[K_LEFT]):
            pygame.mixer.music.stop()
            acceleratelow.play()
            car1.steer(π * 0.3, -1)
        elif(keys_pressed[K_RIGHT]):
            pygame.mixer.music.stop()
            acceleratelow.play()
            car1.steer(π * 0.3, 1)
        else:
             pressedtime = time.time() - start_time
             if pressedtime < 1.2:
                engineidle.play()
             else:
                 engineidle.stop()
             car1.steer(0, 1)
        car1.apply_tire_top_down_friction()
        screen.fill((255, 255, 255))
        world.step(1/60.0)
        world.debug_draw(draw_options)
        pygame.display.flip()
        clock.tick(60)


if __name__ == '__main__':
    sys.exit(main())
