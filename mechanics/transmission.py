#  Copyright © 2018 Anant Sujatanagarjuna
#  This file is part of Need_for_Shit.

#     Need_for_Shit is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     Need_for_Shit is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with Need_for_Shit.  If not, see <https://www.gnu.org/licenses/>.

from numpy import (poly1d,
                   polyfit,
                   mean,
                   array as nparray)


def isPowered(tire):
    return tire.powered


class Transmission(object):
    """Simulate some-what realistic transmission"""

    def __init__(self,
                 vehicle,
                 gear_ratios,
                 final_drive_ratio,
                 max_engine_rpm,
                 torque_curve_points,
                 torque_curve_fit_poly=None,
                 physics_tick_rate=60.0,
                 engine_braking=10,
                 throttle_rpm_acceleration=500,
                 auto_gear_shift_time=100,
                 corr=2.8,
                 engine_momentum=0.6):
        """
        Input Specification :
        _____________________
        vehicle: vehicle_mechanics.Vehicle object

        gear_ratios: should be a list or tuple of gear ratios eg:
        [1, 4.17, 2.34, 1.52, 1.14, 0.86, 0.69, -3.23], ie torque multipliers.
        Reverse gear ratio must be the last entry, and neutral the first. You
        can put any number in the entries except 0

        final_drive_ratio: Final drive gear ratio(torque multiplier)

        max_engine_rpm: Maximum permitted engine rpm

        torque_curve_points: A list of points plotted as (rpm, torque(in Nm)),
        that is representative of the engine's torque curve

        torque_curve_fit_poly (Optional): A numpy.poly1d polynomial that best
        fits the engine's torque curve, or just a function that accepts engine
        rpm as input and returns torque. If not supplied, a polynomial of
        degree 3 will be fitted automatically on instantiation using
        numpy.polyfit

        physics_tick_rate: Frequency(Hz) of physics engine

        engine_braking: How much the engine resists(retards) increase in rpm

        throttle_rpm_acceleration: Acceleration of rpm when applying throttle

        auto_gear_shift_time: Time in milliseconds the automatic transmission
        takes to shift gear

        corr: A small correction factor that over-estimates vehicle
        velocity to make wheel slip more realistic

        engine_momentum: Small fraction, for realistic results, between 0 and
        1. This is the fraction of wheel torque that is applied at all times
        when engine is engaged, even if throttle is 0
        """
        self.vehicle = vehicle
        self.gear_ratios = gear_ratios
        self.final_drive_ratio = final_drive_ratio
        # Calculate Effective gear ratios
        self.eff_gear_r = [gr * self.final_drive_ratio for gr in self.gear_ratios]
        self.inv_eff_gear_r = [1.0/egr for egr in self.eff_gear_r]

        self.max_engine_rpm = max_engine_rpm
        self.torque_curve_points = nparray(torque_curve_points)
        if torque_curve_fit_poly is None:
            self.torque_curve_fit_poly = poly1d(
                polyfit(self.torque_curve_points[:, 0],
                        self.torque_curve_points[:, 1],
                        3))
        else:
            self.torque_curve_fit_poly = torque_curve_fit_poly
        self.physics_tick_rate = physics_tick_rate
        self.phy_time = 1.0/self.physics_tick_rate
        self.engine_braking = engine_braking
        self.throttle_rpm_acceleration = throttle_rpm_acceleration

        self.curr_gear_index = 1
        self.engine_rpm = 0
        self.rpm_range = range(0, self.max_engine_rpm)
        self.drive_wheels = [tire for tn, tire in self.vehicle.Tires.items() if tire.powered]
        self.inv_num = 1.0/len(self.drive_wheels)
        self.avg_wheel_circumference = mean([tire.circumference for tire in self.drive_wheels])
        self.cir_inverse = 1.0 / self.avg_wheel_circumference
        self.avg_grip = mean([tire.tire_grip for tire in self.drive_wheels])
        self.clutch_engaged = False
        self.corr = corr
        self.engine_momentum = engine_momentum

    def drive(self, throttle):
        """
        Rev the engine and drive the wheels.
        throttle: number between 0 and 1
        """
        vlv = self.vehicle.local_velocity.y
        # Is the transmission engaged ?
        tr_engaged = (not (self.clutch_engaged or self.curr_gear_index == 0))
        # Get average tire friction multplier
        avg_f_m = mean([tire.friction_multiplier for tire in self.drive_wheels])
        # Calculate wheel slip
        wheel_slip = tr_engaged * (self.engine_rpm * self.inv_eff_gear_r[self.curr_gear_index] - vlv * self.cir_inverse * self.corr)
        # Calculate slip multiplier
        slip_mult = wheel_slip * self.avg_grip * avg_f_m
        # Note: Using averages here doesnt make differential tire physics
        # obsolete. This is just to *retard* the engine based on how much
        # the wheels are slipping

        # engine_rpm increases in accordance with throttle_rpm_acceleration,
        # but decreases with engine_braking
        # wheel_slip affects engine_rpm in the following manner : If positive,
        # ie wheel rpm is greater than it should be for actual velocity then
        # it decreases engine_rpm, else if wheel_rpm is less than velocity,
        # engine_rpm will increase
        self.engine_rpm = max(0, min(self.engine_rpm + self.phy_time * (self.eff_gear_r[self.curr_gear_index] * (throttle * self.throttle_rpm_acceleration - (throttle == 0) * self.engine_braking) - slip_mult), self.max_engine_rpm))
        # wheel_slip always affects wheel_torque negatively and is
        # applied evenly to all wheels
        wheel_torque = tr_engaged * (self.torque_curve_fit_poly(self.engine_rpm) * self.eff_gear_r[self.curr_gear_index] * self.inv_num - abs(slip_mult)) * (throttle + self.engine_momentum)
        for wheel in self.drive_wheels:
            wheel.drive((0, wheel_torque * wheel.inv_radius * wheel.tire_grip))
