#  Copyright © 2018 Anant Sujatanagarjuna
#  This file is part of Need_for_Shit.

#     Need_for_Shit is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     Need_for_Shit is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with Need_for_Shit.  If not, see <https://www.gnu.org/licenses/>.

import sys
from math import pi as π
from mechanics.vehicle_mechanics import Vehicle
import pygame
import pymunk.pygame_util
from pygame.locals import *

def main():
    pygame.init()
    screen = pygame.display.set_mode((800, 600))
    draw_options = pymunk.pygame_util.DrawOptions(screen)
    pygame.display.set_caption("Need For Shit")
    clock = pygame.time.Clock()
    img = pygame.image.load("bg.jpg")
    world = pymunk.Space()
    world.gravity = (0.0, 0.0)
    car1 = Vehicle(world,
                   (400, 00),
                   mass=20,
                   width=20,
                   length=40,
                   front_tire_width=5,
                   front_tire_grip=0.5,
                   rear_tire_grip=.6,
                   front_tire_length=8,
                   rear_tire_width=5,
                   rear_tire_length=9,
                   throttle_rpm_acceleration=200,
                   engine_braking=300,
                   gear_ratios=[1.0, 4.17, 2.34, 1.52, 1.14, 0.86, 0.69, -3.23],
                   final_drive_ratio=3.0,
                   max_engine_rpm=7000,
                   torque_curve_points=[(0, 0),
                                        (1000, 250),
                                        (1500, 375),
                                        (2000, 437),
                                        (2500, 475),
                                        (3000, 500),
                                        (3500, 525),
                                        (4000, 540),
                                        (4500, 540),
                                        (5000, 530),
                                        (5500, 525),
                                        (6000, 500),
                                        (6500, 475),
                                        (7000, 400)])
    while True:
        throttle = 0
        sys.stdout.flush()
        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                sys.exit(0)
        keys_pressed = pygame.key.get_pressed()
        if(keys_pressed[K_UP]):
            throttle = 1
        else:
            throttle = 0
        if(keys_pressed[K_DOWN]):
            car1.brake()

        if(keys_pressed[K_LEFT]):
            car1.steer(π * 0.3, -1)
        elif(keys_pressed[K_RIGHT]):
            car1.steer(π * 0.3, 1)
        else:
            car1.steer(0, 1)
        car1.drive(throttle)
        car1.apply_tire_top_down_friction()
        screen.fill((255, 255, 255))
        # screen.blit(img, (0, 0))
        world.step(1/60.0)
        world.debug_draw(draw_options)
        pygame.display.flip()
        clock.tick(60)


if __name__ == '__main__':
    sys.exit(main())
